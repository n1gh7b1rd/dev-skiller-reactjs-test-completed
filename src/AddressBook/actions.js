import { actions as searchActions } from "./SearchContacts";
import { actions as contactDetailsActions } from "./ContactDetails";

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

const tmp = (dispatch, httpApi, newPhrase) => {
    httpApi.getFirst5MatchingContacts({ namePart: newPhrase })
    .then(({ data }) => {
        const matchingContacts = data.map(contact => ({
        id: contact.id,
        value: contact.name,
        }));
        // TODOx something is wrong here
        dispatch(
        searchActions.updateSearchPhraseSuccess({ matchingContacts: matchingContacts }),
        );
    })
    .catch(() => {
        searchActions.updateSearchPhraseFailure()
    });
}


const debouncedTmp = debounce(tmp, 300);

export const updateSearchPhrase = newPhrase =>
  (dispatch, getState, { httpApi }) => {
    dispatch(
      searchActions.updateSearchPhraseStart({ newPhrase }),
    );

    debouncedTmp(dispatch, httpApi, newPhrase);
  };

export const selectMatchingContact = selectedMatchingContact =>
  (dispatch, getState, { httpApi, dataCache }) => {

    // TODO something is missing here
    const getContactDetails = ({ id }) => {
      return httpApi
          .getContact({ contactId: selectedMatchingContact.id })
          .then(({ data }) => ({
            id: data.id,
            name: data.name,
            phone: data.phone,
            addressLines: data.addressLines,
          }));
    };

    dispatch(
      searchActions.selectMatchingContact({ selectedMatchingContact }),
    );

    dispatch(
      contactDetailsActions.fetchContactDetailsStart(),
    );

    const cached = dataCache.load({key: selectedMatchingContact.id});
    if(cached){
        dispatch(contactDetailsActions.fetchContactDetailsSuccess({contactDetails: cached}));
    }
    else {
        getContactDetails({ id: selectedMatchingContact.id })
        .then((contactDetails) => {
            // TODOx? something is missing here
            dataCache.store({
            key: contactDetails.id,
            value: contactDetails
            });
            // TODOx something is wrong here
            dispatch(
            contactDetailsActions.fetchContactDetailsSuccess({contactDetails}),
            );
        })
        .catch(() => {
            dispatch(
            contactDetailsActions.fetchContactDetailsFailure(),
            );
        });
    }
  };